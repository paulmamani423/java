/*Escribir un programa que imprima los "N" numeros y la salida sea de la siguirente manera
1 2 3 4 5 ... N*/
import java.util.Scanner;
public class Ejercicio7 {
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    System.out.print("Ingrese el numero: ");
    int n = input.nextInt();
    for (int i = 1; i <= n; i++) {
      System.out.print(i + " ");
    }
  }
}
