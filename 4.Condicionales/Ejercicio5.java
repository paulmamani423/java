/*Desarrollar un programa que imprima la siguiente salida:
            *
           ***
          *****
         *******
          *****
           ***
            * */ 
 
public class Ejercicio5 {
        public static void main(String[] args) {
        int n = 4;
            
            for (int i = 0; i < n; i++) {
                for (int j = n - i; j > 1; j--) {
                    System.out.print(" ");
                }
                for (int j = 0; j <= i * 2; j++) {
                    System.out.print("*");
                }
                    System.out.println();
            }
            
            for (int i = n - 2; i >= 0; i--) {
                for (int j = n - i; j > 1; j--) {
                    System.out.print(" ");
                }
                for (int j = 0; j <= i * 2; j++) {
                    System.out.print("*");
                }
                    System.out.println();
            }
        }
}
            
            