//Escribir un programa que permita calcular el Mínimo Común Multiplo de "N" numeros enpleando el bucle for.
import java.util.Scanner;

public class Ejercicio8 {
   public static void main(String[] args) {
       Scanner input = new Scanner(System.in);
       int n, a, b, var;
       System.out.print("Ingrese cuantos numeros a calcular: ");
       n = input.nextInt();
       System.out.print("Ingrese numero: ");
       a = input.nextInt();
       var = a;
       for (int i = 1; i < n; i++) {
           System.out.print("Ingrese siguiente numero: ");
           b = input.nextInt();
           while (b != 0) {
               int temp = b;
               b = var % b;
               var = temp;
           }
       }
       System.out.println("El Máximo Común Divisor es: " + var);
   }
}
