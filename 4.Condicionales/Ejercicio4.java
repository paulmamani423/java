/*Desarollar un programa que imprima la siguiente salida:
*
**
***
****
*****
******
*******
******** */
public class Ejercicio4 {
    public static void main(String[] args) {
        for (int i = 1; i <= 8; i++){
            for (int j = 1; j <= i; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
        
    }
    
}

//Esta es la otra parte para que ingresando el numero te compile la cantidad.

/*import java.util.Scanner;
public class Ejercicio4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un número entero: ");
        int num = sc.nextInt();
        for (int i = 1; i <= num; i++){
            for (int j = 1; j <= i; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
        
    }
    
}*/