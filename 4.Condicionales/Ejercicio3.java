//Escribir un programa que calcule la suma de los primeros n números perfectos, donde "N" es un número que el usuario ingresa por teclado.
import java.util.Scanner;
public class Ejercicio3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Ingrese primer numero n: ");
        int n = input.nextInt();

        int count = 0;
        int sum = 0;
        int num = 2;
        
        while (count < n) {
            if (esPerfecto(num)) {
                sum += num;
                count++;
            }
            num++;
        }
        
        System.out.println("La suma de los primeros " + n + " números perfectos es: " + sum);
    }
    
    public static boolean esPerfecto(int num) {
        int sum = 1;
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                sum += i;
                if (i != num / i) {
                    sum += num / i;
                }
            }
        }
        return sum == num;
    }
}
