/*Escriba un programa que dibuje un cuadrado donde los lados se pintarán de la siguiente forma:
Por ejemplo si el lado es 7, se debe dibujar de la siguiente forma:

* * * * * * *
* * * * * * * 
* * * * * * *
* * * * * * *
* * * * * * *
* * * * * * * */

public class Ejercicio6 {
    public static void main(String[] args) {
        System.out.println("Por ejemplo si el lado es 7, se debe dibujar de la siguiente forma: ");
        int lado = 7;
        for (int i = 0; i < lado; i++) {
            for (int j = 0; j < lado; j++) {
                if (i % 1 == 0) {
                    if (j % 1 == 0) {
                        System.out.print(" * ");
                    } else {
                        System.out.print(" ");
                    }
                } else {
                    if (j % 1 == 0) {
                        System.out.print(" * ");
                    } else {
                        System.out.print(" ");
                    }
                }
            }
            System.out.println();
        }
    } 
}
