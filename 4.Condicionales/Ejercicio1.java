//Escribir un programa que imprima la secuencia de Fibonacci hasta un número específico, utilizando un bucle while.
import java.util.Scanner;
public class Ejercicio1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un número: ");
        int num = sc.nextInt();
        int a = 0, b = 1, c = 0;
        while (c <= num) {
            System.out.print(c + " ");
            a = b;
            b = c;
            c = a + b;
        }
        sc.close();
    }
}
