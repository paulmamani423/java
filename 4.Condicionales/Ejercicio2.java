//Desarrollar un programa que sume los "N" números impares.
import java.util.Scanner;

public class Ejercicio2 {

    public static void main(String[] args) {
        
        Scanner entrada = new Scanner(System.in);
        int n, sum = 0;
        
        System.out.print("Ingrese el valor de N: ");
        n = entrada.nextInt();
        
        for (int i = 1; i <= 2*n; i++) {
            if (i % 2 != 0) {
                sum += i;
            }
        }
        
        System.out.println("La suma de los " + n + " números impares es: " + sum);
    }
}
