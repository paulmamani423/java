//Calcular el máximo común divisor de dos números de N números.
import java.util.Scanner;
public class Ejercicio2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese la cantidad de numeros a calcular: ");
        int n = scanner.nextInt();
        int[] numeros = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("Ingrese " + (i+1) + " numero: ");
            numeros[i] = scanner.nextInt();
        }
        scanner.close();
        int mcd = calcularMCD(numeros, 0, n-1);
        System.out.println("El MCD de los números ingresados es: " + mcd);
    }
    public static int calcularMCD(int[] numeros, int inicio, int fin) {
        if (inicio == fin) {
            return numeros[inicio];
        } else {
            int mitad = (inicio + fin) / 2;
            int mcdIzquierdo = calcularMCD(numeros, inicio, mitad);
            int mcdDerecho = calcularMCD(numeros, mitad+1, fin);
            return calcularMCD(mcdIzquierdo, mcdDerecho);
        }
    }
    public static int calcularMCD(int a, int b) {
        if (b == 0) {
            return a;
        } else {
            return calcularMCD(b, a % b);
        }
    }
}
