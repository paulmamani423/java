//Calcular el número de permutaciones de N elementos de una lista.
import java.math.BigInteger;
import java.util.Scanner;
public class Ejercicio4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese numero:");
        int n = sc.nextInt();
        BigInteger permutaciones = calcularPermutaciones(n);
        System.out.println("El número de permutaciones es: " + permutaciones);
        sc.close();
    }
    public static BigInteger calcularPermutaciones(int n) {
        if (n == 1) {
            return BigInteger.ONE;
        } else {
            return BigInteger.valueOf(n).multiply(calcularPermutaciones(n-1));
        }
    }
    
}
