//Calcular el n-ésimo término de la serie de Fibonacci.
import java.util.Scanner;
import java.math.BigInteger;
public class Ejercicio1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese el número de términos de la serie de Fibonacci que desea calcular: ");
        int n = scanner.nextInt();
        scanner.close();
        BigInteger[] fib = new BigInteger[n+1];
        fib[0] = BigInteger.ZERO;
        fib[1] = BigInteger.ONE;
        for (int i = 2; i <= n; i++) {
            fib[i] = fib[i-1].add(fib[i-2]);
        }
        System.out.println("El " + n + "numero de término de la serie de Fibonacci es: " + fib[n]);
    }
}
