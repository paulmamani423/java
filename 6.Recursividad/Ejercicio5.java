//Crear una clase "Fractal" que tenga como atributos el tipo de fractal y los parámetros necesarios para su construcción, y como métodos la construcción del fractal utilizando recursividad.
import java.util.Scanner;

public class Ejercicio5 {
    private String tipo;
    private int iteraciones;
    private double factorEscala;

    public Ejercicio5(String tipo, int iteraciones, double factorEscala) {
        this.tipo = tipo;
        this.iteraciones = iteraciones;
        this.factorEscala = factorEscala;
    }

    public void construir() {
        if (tipo.equals("Arbol")) {
            construirArbol(iteraciones, 100, 200, -90, factorEscala);
        } else if (tipo.equals("Copos de Nieve")) {
            construirCoposDeNieve(iteraciones, 300, 300, 200, factorEscala);
        } else if (tipo.equals("Triangulo de Sierpinski")) {
            construirTrianguloDeSierpinski(iteraciones, 100, 300, 500, 300, factorEscala);
        } else {
            System.out.println("Fractal no soportado");
        }
    }

    private void construirArbol(int iteraciones, double x, double y, double angulo, double factorEscala) {
        if (iteraciones > 0) {
            double x2 = x + Math.cos(Math.toRadians(angulo)) * iteraciones * factorEscala;
            double y2 = y + Math.sin(Math.toRadians(angulo)) * iteraciones * factorEscala;
            construirArbol(iteraciones - 1, x2, y2, angulo - 20, factorEscala);
            construirArbol(iteraciones - 1, x2, y2, angulo + 20, factorEscala);
        }
    }

    private void construirCoposDeNieve(int iteraciones, double x, double y, double size, double factorEscala) {
        if (iteraciones == 0) {
        } else {
            double newSize = size / 3;
            construirCoposDeNieve(iteraciones - 1, x, y, newSize, factorEscala);
            construirCoposDeNieve(iteraciones - 1, x + newSize, y, newSize, factorEscala);
            construirCoposDeNieve(iteraciones - 1, x + (newSize / 2), y + (newSize * Math.sqrt(3) / 2), newSize, factorEscala);
            construirCoposDeNieve(iteraciones - 1, x + newSize, y, newSize, factorEscala);
        }
    }

    private void construirTrianguloDeSierpinski(int iteraciones, double x1, double y1, double x2, double y2, double factorEscala) {
        if (iteraciones == 0) {
        } else {
            double x3 = (x1 + x2) / 2;
            double y3 = (y1 + y2) / 2;
            construirTrianguloDeSierpinski(iteraciones - 1, x1, y1, x3, y3, factorEscala);
            construirTrianguloDeSierpinski(iteraciones - 1, x3, y3, x2, y2, factorEscala);
            construirTrianguloDeSierpinski(iteraciones - 1, x1, y1, x2, y2, factorEscala);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese el tipo de fractal (Arbol, Copos de Nieve, Triangulo de Sierpinski):");
        String tipo = scanner.nextLine();
        System.out.println("Ingrese el número de iteraciones:");
        int iteraciones = scanner.nextInt();
        System.out.println("Ingrese el factor de escala:");
        double factorEscala = scanner.nextDouble();
    
        Ejercicio5 fractal = new Ejercicio5(tipo, iteraciones, factorEscala);
        fractal.construir();
    
        // Imprimir el resultado
        System.out.println("Fractal generado:");
        System.out.println(fractal.toString());
    }
}    