//Crear un programa que calcule el valor, máximo, mínimo, el promedio, la moda y la desviación estandar de 1000 datos que se generen aleatoriamente con un rango de 0 a 300 elementos, los datos deben ser generados en un archivo datos_generados.txt,seguidamente se debe generar y los resultados para ser guardados en un archivo resultados.txt
import java.io.*;
import java.util.*;

public class Ejercicio6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        ArrayList<Integer> datos = new ArrayList<>();
        
        System.out.print("Ingrese el número de datos: ");
        int n = input.nextInt();
        
        System.out.println("Ingrese los " + n + " datos:");
        for (int i = 0; i < n; i++) {
            int num = input.nextInt();
            datos.add(num);
        }
        
        input.close();
        calcularResultados(datos);
    }
    
    public static void calcularResultados(ArrayList<Integer> datos) {
        int max = Collections.max(datos);
        int min = Collections.min(datos);
        double sum = 0;
        for (int num : datos) {
            sum += num;
        }
        double promedio = sum / datos.size();
        int moda = 0;
        int frecuenciaMaxima = 0;
        for (int i = 0; i < datos.size(); i++) {
            int frecuencia = Collections.frequency(datos, datos.get(i));
            if (frecuencia > frecuenciaMaxima) {
                frecuenciaMaxima = frecuencia;
                moda = datos.get(i);
            }
        }
        double desviacion = 0;
        for (int num : datos) {
            desviacion += Math.pow(num - promedio, 2);
        }
        desviacion = Math.sqrt(desviacion / datos.size());
        try {
            FileWriter fw = new FileWriter("resultados.txt");
            fw.write("Valor maximo: " + max + "\n");
            fw.write("Valor minimo: " + min + "\n");
            fw.write("Promedio: " + promedio + "\n");
            fw.write("Moda: " + moda + "\n");
            fw.write("Desviacion estandar: " + desviacion + "\n");
            fw.close();
            System.out.println("Los resultados se han guardado en el archivo 'resultados.txt'.");
        } catch (IOException e) {
            System.out.println("Error al guardar los resultados.");
        }
    }
}

