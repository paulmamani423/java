//Calcular el mínimo común múltiplo de dos números de N números.
import java.util.Scanner;
public class Ejercicio3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese cantidad de numeros a calcular: ");
        int n = scanner.nextInt();
        int[] numeros = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("Ingrese " + (i+1) + " numero: ");
            numeros[i] = scanner.nextInt();
        }
        scanner.close();
        int mcm = calcularMCM(numeros, 0, n-1);
        System.out.println("El MCM de los números ingresados es: " + mcm);
    }
    public static int calcularMCM(int[] numeros, int inicio, int fin) {
        if (inicio == fin) {
            return numeros[inicio];
        } else {
            int mitad = (inicio + fin) / 2;
            int mcmIzquierdo = calcularMCM(numeros, inicio, mitad);
            int mcmDerecho = calcularMCM(numeros, mitad+1, fin);
            return calcularMCM(mcmIzquierdo, mcmDerecho);
        }
    }
    public static int calcularMCM(int a, int b) {
        return a * b / calcularMCD(a, b);
    } 
    public static int calcularMCD(int a, int b) {
        if (b == 0) {
            return a;
        } else {
            return calcularMCD(b, a % b);
        }
    }
}

