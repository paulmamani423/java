//Desarrollar un algoritmo recursivo para realizar un ordenamiento de los datos generados empleando "QuickSort".
import java.util.*;

public class Ejercicio7 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese la cantidad de números a ordenar: ");
        int n = sc.nextInt();

        int[] numeros = new int[n];
        System.out.println("Ingrese los números a ordenar:");
        for (int i = 0; i < n; i++) {
            numeros[i] = sc.nextInt();
        }

        quickSort(numeros, 0, n-1);

        System.out.println("Los números ordenados son:");
        for (int i = 0; i < n; i++) {
            System.out.print(numeros[i] + " ");
        }
    }

    public static void quickSort(int[] arr, int low, int high) {
        if (low < high) {
            int pi = partition(arr, low, high);

            quickSort(arr, low, pi - 1);
            quickSort(arr, pi + 1, high);
        }
    }

    public static int partition(int[] arr, int low, int high) {
        int pivot = arr[high];
        int i = low - 1;

        for (int j = low; j <= high - 1; j++) {
            if (arr[j] < pivot) {
                i++;
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }

        int temp = arr[i + 1];
        arr[i + 1] = arr[high];
        arr[high] = temp;

        return i + 1;
    }
}
