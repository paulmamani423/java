//Agregar un método para determinar si un vehículo tiene multas y otro método para determinar el valor total de multas de un vehículo.
import java.util.ArrayList;
import java.util.Scanner;
public class Ejercicio3 {
    private String placa;
    private String marca;
    private int velocidadMaxima;
    private int velocidadActual;
    private int multa;
    private ArrayList<Integer> multas;
    public Ejercicio3(String placa, String marca, int velocidadMaxima) {
        this.placa = placa;
        this.marca = marca;
        this.velocidadMaxima = velocidadMaxima;
        this.velocidadActual = 0;
        this.multa = 0;
        this.multas = new ArrayList<Integer>();
    }
    public void acelerar(int velocidad) {
        this.velocidadActual += velocidad;
        if (this.velocidadActual > this.velocidadMaxima) {
            int exceso = this.velocidadActual - this.velocidadMaxima;
            int nuevaMulta = exceso * 10000;
            this.multa += nuevaMulta;
            this.multas.add(nuevaMulta);
            System.out.println("Multa generada: $" + nuevaMulta);
        }
    }
    public boolean tieneMultas() {
        return !this.multas.isEmpty();
    }
    public int valorTotalMultas() {
        int total = 0;
        for (int m : this.multas) {
            total += m;
        }
        return total;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese la placa del vehículo: ");
        String placa = scanner.nextLine();
        System.out.print("Ingrese la marca del vehículo: ");
        String marca = scanner.nextLine();
        System.out.print("Ingrese la velocidad máxima del vehículo: ");
        int velocidadMaxima = scanner.nextInt();
        Ejercicio3 vehiculo = new Ejercicio3(placa, marca, velocidadMaxima);
        boolean salir = false;
        while (!salir) {
            System.out.println("\nVelocidad actual: " + vehiculo.velocidadActual);
            System.out.print("Ingrese la velocidad a la que desea acelerar o '-1' para salir: ");
            int velocidad = scanner.nextInt();
            if (velocidad == -1) {
                salir = true;
            } else {
                vehiculo.acelerar(velocidad);
                System.out.println("Multa acumulada: $" + vehiculo.multa);
            }
        }
        System.out.println("\nResumen de multas:");
        if (vehiculo.tieneMultas()) {
            System.out.println("El vehículo tiene multas por un total de $" + vehiculo.valorTotalMultas());
            System.out.println("Detalle de multas: " + vehiculo.multas.toString());
        } else {
            System.out.println("El vehículo no tiene multas.");
        }
    }
}



