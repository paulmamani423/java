//Agregar a la clase Automóvil, un atributo para determinar si el vehículo es automático o no. Agregar los métodos get y set para dicho atributo. Modificar el constructor para inicializar dicho atributo.
import java.util.Scanner;

public class Ejercicio1 {
    private boolean esAutomatico;

    public Ejercicio1(boolean esAutomatico) {
        this.esAutomatico = esAutomatico;
    }

    public boolean getEsAutomatico() {
        return esAutomatico;
    }

    public void setEsAutomatico(boolean esAutomatico) {
        this.esAutomatico = esAutomatico;
    }
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Ingrese si el automóvil es automático (true o false): ");
        boolean esAutomatico = input.nextBoolean();
        Ejercicio1 miAuto = new Ejercicio1(esAutomatico);
        System.out.println("El automóvil es automático? " + miAuto.getEsAutomatico());

        /*System.out.print("Ingrese si el automóvil es automático (verdadero o falso): ");
        esAutomatico = input.nextBoolean();
        miAuto.setEsAutomatico(esAutomatico);
        System.out.println("El automóvil es automático? " + miAuto.getEsAutomatico());*/

    }
}


