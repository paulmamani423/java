//Agregar una nueva clase denominada Trapecio. Definir los métodos para calcular el área y el perímetro de esta nueva figura geométrica.
import java.util.Scanner;

public class Ejercicio5 {
    private double baseMayor;
    private double baseMenor;
    private double altura;
    private double lado1;
    private double lado2;

    public Ejercicio5(double baseMayor, double baseMenor, double altura, double lado1, double lado2) {
        this.baseMayor = baseMayor;
        this.baseMenor = baseMenor;
        this.altura = altura;
        this.lado1 = lado1;
        this.lado2 = lado2;
    }

    public double calcularArea() {
        return ((baseMayor + baseMenor) / 2) * altura;
    }

    public double calcularPerimetro() {
        return baseMayor + baseMenor + lado1 + lado2;
    }

    public static void main(String[] args) {
        Scanner xd = new Scanner(System.in);

        System.out.println("Ingrese base mayor del trapecio:");
        double baseMayor = xd.nextDouble();

        System.out.println("Ingrese base menor del trapecio:");
        double baseMenor = xd.nextDouble();

        System.out.println("Ingrese altura del trapecio:");
        double altura = xd.nextDouble();

        System.out.println("Ingrese primer lado del trapecio:");
        double lado1 = xd.nextDouble();

        System.out.println("Ingrese segundo lado del trapecio:");
        double lado2 = xd.nextDouble();

        Ejercicio5 t = new Ejercicio5(baseMayor, baseMenor, altura, lado1, lado2);

        System.out.println("El area del trapecio es: " + t.calcularArea());
        System.out.println("El perimetro del trapecio es: " + t.calcularPerimetro());
    }
}
