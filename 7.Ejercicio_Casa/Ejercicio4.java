//Agregar una nueva clase denominada Rombo. Definir los métodos para calcular el área y el perímetro de esta nueva figura geométrica.
import java.util.Scanner;
public class Ejercicio4 {
    private double diagonalMayor;
    private double diagonalMenor;
    public Ejercicio4(double diagonalMayor, double diagonalMenor) {
        this.diagonalMayor = diagonalMayor;
        this.diagonalMenor = diagonalMenor;
    }
    public double calcularArea() {
        return (diagonalMayor * diagonalMenor) / 2;
    }
    public double calcularPerimetro() {
        return 2 * Math.sqrt(Math.pow(diagonalMayor / 2, 2) + Math.pow(diagonalMenor / 2, 2));
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese el valor de la diagonal mayor: ");
        double diagonalMayor = scanner.nextDouble();
        System.out.print("Ingrese el valor de la diagonal menor: ");
        double diagonalMenor = scanner.nextDouble();
        Ejercicio4 rombo = new Ejercicio4(diagonalMayor, diagonalMenor);
        double area = rombo.calcularArea();
        double perimetro = rombo.calcularPerimetro();
        System.out.println("El área del rombo es: " + area);
        System.out.println("El perímetro del rombo es: " + perimetro);
    }
}
