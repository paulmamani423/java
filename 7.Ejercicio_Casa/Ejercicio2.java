//Modificar el método acelerar para que si la velocidad máxima se sobrepase se genere una multa. Dicha multa se puede incrementar cada vez que el vehículo intenta superar la velocidad máxima permitida.
import java.util.Scanner;

public class Ejercicio2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int velocidadLimite;
        int velocidadActual;
        int multa = 0;

        System.out.print("Ingrese la velocidad límite: ");
        velocidadLimite = scanner.nextInt();

        System.out.print("Ingrese la velocidad actual: ");
        velocidadActual = scanner.nextInt();

        if (velocidadActual > velocidadLimite) {
            System.out.println("Se ha detectado un exceso de velocidad.");
            multa = 100;
            while (velocidadActual > velocidadLimite) {
                multa += 50;
                velocidadActual -= 10;
            }
            System.out.println("Multa generada: $" + multa);
        } else {
            System.out.println("La velocidad se encuentra dentro de los límites permitidos.");
        }
    }
}

