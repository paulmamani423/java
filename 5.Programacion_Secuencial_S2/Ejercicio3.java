//Crear un método que encuentre el elemento máximo en un arreglo de enteros.
import java.util.Scanner;
public class Ejercicio3 {
    public static void main(String[] args) {
        Scanner max = new Scanner(System.in);
        System.out.print("¿Cuantos numeros ingresaras?: ");
        int cantidad = max.nextInt();
        int[] arreglo = new int[cantidad];
        for (int i = 0; i < cantidad; i++) {
            System.out.print("Ingrese " + (i + 1) + " numero: ");
            arreglo[i] = max.nextInt();
        }
        int maximo = encontrarMaximo(arreglo);
        System.out.println("El numero máximo es: " + maximo);
    }
    public static int encontrarMaximo(int[] arreglo) {
        int maximo = arreglo[0];
        for (int i = 1; i < arreglo.length; i++) {
            if (arreglo[i] > maximo) {
                maximo = arreglo[i];
            }
        }
        return maximo;
    }
}
  




