/*Leer un numero entre 1000 a 7000 y almacenar los digitos en un array e imprimirlos en pantalla de la siguiente manera:
Ingresa el número: 5501
Salida:
[3] = 5
[2] = 5
[1] = 0
[0] = 1*/
import java.util.Scanner;
public class Ejercicio5 {
    public static void main(String[] args) {
        int[] digits = new int[4];
        Scanner input = new Scanner(System.in);
        System.out.print("Ingresa el número entre 1000 y 7000: ");
        int number = input.nextInt();
        for (int i = 0; i < 4; i++) {
            digits[i] = number % 10;
            number /= 10;
        }
        for (int i = 3; i >= 0; i--) {
            System.out.printf("[%d] = %d\n", i, digits[i]);
        }
    }
}
