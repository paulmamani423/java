/*Crear un programa que permita crear el factorial de "N" números grandes (ejemplo 50) menores o iguales que 100; todos estos resultados deben ser almacenados en un array, finalmente el programa debe permitir imprimirlos en pantalla.
¿Cuántos números ingresará?: 3
Salida:
El factorial de 5 es: 120
El factorial de 10 es: 3628800
El factorial de 10 es: 93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000*/
import java.math.BigInteger;
import java.util.Scanner;
public class Ejercicio4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("¿Cuántos números ingresará?: ");
        int n = scanner.nextInt();
        BigInteger[] factorials = new BigInteger[n];
        for (int i = 0; i < n; i++) {
            System.out.print("Ingrese el número " + (i+1) + ": ");
            int num = scanner.nextInt();
            factorials[i] = factorial(num);
        }
        System.out.println("Los factoriales son:");
        for (int i = 0; i < n; i++) {
            System.out.println("El factorial de " + (i+1) + " es: " + factorials[i]);
        }
    }
    private static BigInteger factorial(int n) {
        if (n < 0 || n > 100) {
            throw new IllegalArgumentException("n debe estar en el rango [0, 100]");
        }
        BigInteger[] factorials = new BigInteger[101];
        factorials[0] = BigInteger.ONE;
        for (int i = 1; i <= 100; i++) {
            factorials[i] = factorials[i-1].multiply(BigInteger.valueOf(i));
        }
        return factorials[n];
    }
}
  

// metodo para hallar solo uno por uno 


/*import java.util.Scanner;

public class Ejercicio4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese un número para calcular su factorial: ");
        int n = scanner.nextInt();
        int factorial = 1;
        for (int i = 1; i <= n; i++) {
            factorial *= i;
        }
        System.out.println("El factorial de " + n + " es: " + factorial);
    }
}*/

