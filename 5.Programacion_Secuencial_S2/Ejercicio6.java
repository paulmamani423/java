//Crear un programa que permita convertit un número a números romanos.
import java.util.Scanner;
public class Ejercicio6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Ingresa un número entero positivo: ");
        int number = input.nextInt();

        String roman = convertToRoman(number);
        System.out.printf("%d en números romanos es: %s", number, roman);
    }
    public static String convertToRoman(int number) {
        if (number < 1 || number > 3999) {
            throw new IllegalArgumentException("El número debe estar entre 1 y 3999");
        }
        String[] thousands = {"", "M", "MM", "MMM"};
        String[] hundreds = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
        String[] tens = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
        String[] units = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};
        int thousandsDigit = number / 1000;
        int hundredsDigit = (number % 1000) / 100;
        int tensDigit = (number % 100) / 10;
        int unitsDigit = number % 10;
        String roman = thousands[thousandsDigit] + hundreds[hundredsDigit] + tens[tensDigit] + units[unitsDigit];
        return roman;
    }
}
