//Crear un método que calcule el producto de los elementos de un arreglo de enteros.
import java.util.Scanner;
public class Ejercicio2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("¿Cuantos numeros ingresaras?: ");
        int longitud = scanner.nextInt();
        int[] arreglo = new int[longitud];
        for (int i = 0; i < longitud; i++) {
            System.out.print("Ingrese " + (i+1) + " numero: ");
            arreglo[i] = scanner.nextInt();
        }
        int producto = productoArreglo(arreglo);
        System.out.println("El producto de los elementos del arreglo es: " + producto);
    }
    public static int productoArreglo(int[] arreglo) {
        int producto = 1;
        for (int i = 0; i < arreglo.length; i++) {
            producto *= arreglo[i];
        }
        return producto;
    }
}
