//Crear un método que calcule la suma de los elementos de un arreglo de enteros.
import java.util.Scanner;
public class Ejercicio1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese la cantidad numeros para el arreglo:");
        int n = scanner.nextInt();
        int[] arreglo = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.println("Ingrese " + (i + 1) + " numero para el arreglo:");
            arreglo[i] = scanner.nextInt();
        }
        scanner.close();
        int suma = sumarArreglo(arreglo);
        System.out.println("La suma del arreglo es: " + suma);
    }
    public static int sumarArreglo(int[] arreglo) {
        int suma = 0;
        for (int i = 0; i < arreglo.length; i++) {
            suma += arreglo[i];
        }
        return suma;
    }
}







