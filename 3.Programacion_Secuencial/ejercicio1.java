//Determinar si un número entero es primo.
import java.util.Scanner;

public class ejercicio1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un número entero: ");
        int numero = sc.nextInt();
        if (numero <= 1) {
            System.out.println(numero + " no es primo");
            return;
        }
        int limiteSuperior = (int) Math.sqrt(numero);
        for (int i = 2; i <= limiteSuperior; i++) {
            if (numero % i == 0) {
                System.out.println(numero + " no es primo");
                return;
            }
        }
        System.out.println(numero + " es primo");
    }
}
