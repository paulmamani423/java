//En un hospital existen 3 áreas: Oncología, Pediatría y Traumatología. El presupuesto anual del hospital se reparte de la siguiente manera:Área Presupuesto:
public class ejercicio6 {
    double presupuestoOncologia;
    double presupuestoPediatría;
    double presupuestoTraumatología;
    
    public ejercicio6(double presupuestoAnual) {
        this.presupuestoOncologia = presupuestoAnual * 0.55;
        this.presupuestoPediatría = presupuestoAnual * 0.20;
        this.presupuestoTraumatología = presupuestoAnual * 0.25;
    }
    
    public static void main(String[] args) {
        ejercicio6 hospital = new ejercicio6(1000000); 
        System.out.println("Oncología: $" + hospital.presupuestoOncologia);
        System.out.println("Pediatría: $" + hospital.presupuestoPediatría);
        System.out.println("Traumatología: $" + hospital.presupuestoTraumatología);
    }
}
