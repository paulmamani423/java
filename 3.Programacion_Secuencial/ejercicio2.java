//Calcular la raíz cuadrada de un número entero.
import java.util.Scanner;

public class ejercicio2{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese un número entero: ");
        int num = scanner.nextInt();
        double raiz = Math.sqrt(num); 
        System.out.println("La raíz cuadrada de " + num + " es " + raiz);
    }
}
