//Calcular el área de un trapecio a partir de sus cuatro lados.
import java.util.Scanner;

public class ejercicio4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el lado A: ");
        double ladoA = sc.nextDouble();
        System.out.print("Ingrese el lado B: ");
        double ladoB = sc.nextDouble();
        System.out.print("Ingrese el lado C: ");
        double ladoC = sc.nextDouble();
        System.out.print("Ingrese el lado D: ");
        double ladoD = sc.nextDouble();

        // Calcular la altura del trapecio
        double altura = Math.sqrt(Math.pow(ladoB - ladoC, 2) - Math.pow((ladoA - ladoD) / 2, 2));

        // Calcular el área del trapecio
        double area = (ladoA + ladoB) * altura / 2;

        System.out.println("El área del trapecio es: " + area);
    }
}
