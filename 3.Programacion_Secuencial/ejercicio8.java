//Una tienda ofrece un descuento del 15% sobre el total de la compra y un cliente desea saber cuánto deberá pagar finalmente por su compra.
import java.util.Scanner;

public class ejercicio8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese el monto total de la compra: ");
        double montoTotal = scanner.nextDouble();
        double descuento = montoTotal * 0.15;
        double precioFinal = montoTotal - descuento;
        System.out.println("El monto total de la compra es: $" + montoTotal);
        System.out.println("El descuento aplicado es de: $" + descuento);
        System.out.println("El precio final a pagar es: $" + precioFinal);
    }
}
