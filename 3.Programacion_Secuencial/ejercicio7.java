/*Escriba un programa que realice la conversión de grados Celsius (°C) a grados Fahrenheit (°F). La ecuación de conversión es:
F=9/5∗C+32*/
import java.util.Scanner;

public class ejercicio7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese la temperatura en grados Celsius: ");
        double celsius = scanner.nextDouble(); 
        double fahrenheit = (celsius * 9/5) + 32;
        System.out.println(celsius + " grados Celsius equivalen a " + fahrenheit + " grados Fahrenheit.");
    }
}

