//Realizar un algoritmo que permita verificar si los lados ingresados forman y triangulo rectángulo, si cumple con esta condicion permita imprimir sus razones trigonométricas.
import java.util.Scanner;

public class ejercicio5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el lado a: ");
        double a = sc.nextDouble();
        System.out.print("Ingrese el lado b: ");
        double b = sc.nextDouble();
        System.out.print("Ingrese el lado c: ");
        double c = sc.nextDouble();
        if (a + b > c && b + c > a && c + a > b) {
            if (a * a + b * b == c * c || b * b + c * c == a * a || c * c + a * a == b * b) {
                System.out.println("Los lados ingresados forman un triángulo rectángulo");
                double senoA = a / c;
                double cosenoA = b / c;
                double tangenteA = a / b;

                System.out.println("Seno de A: " + senoA);
                System.out.println("Coseno de A: " + cosenoA);
                System.out.println("Tangente de A: " + tangenteA);
            } else {
                System.out.println("Los lados ingresados forman un triángulo, pero no es rectángulo");
            }
        } else {
            System.out.println("Los lados ingresados no forman un triángulo");
        }
    }
}