//Crea un programa que permita al usuario ingresar dos números enteros y determine si son iguales o diferentes.

import java.util.Scanner;
public class Igualdad {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese el primer número entero: ");
        int numero1 = scanner.nextInt();
        System.out.print("Ingrese el segundo número entero: ");
        int numero2 = scanner.nextInt();
        if (numero1 == numero2) {
            System.out.println("Los números ingresados son iguales.");
        } else {
            System.out.println("Los números ingresados son diferentes.");
        }
    }
}
