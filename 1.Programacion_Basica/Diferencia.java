//Crea un programa que permita al usuario ingresar dos números enteros y calcule su diferencia.

import java.util.Scanner;
public class Diferencia {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingresa un número: ");
        int num1 = scanner.nextInt();

        System.out.print("Ingresa otro número: ");
        int num2 = scanner.nextInt();

        int resultado = num1 - num2;

        System.out.println("La diferencia entre " + num1 + " y " + num2 + " es: " + resultado);
    }
}
