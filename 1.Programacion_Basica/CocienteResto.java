//Crea un programa que permita al usuario ingresar dos números enteros y calcule su cociente y su resto.

import java.util.Scanner;
public class CocienteResto {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingresa primer número: ");
        int num1 = scanner.nextInt();

        System.out.print("Ingresa segundo número: ");
        int num2 = scanner.nextInt();

        int cociente = num1 / num2;
        int resto = num1 % num2;

        System.out.println("El cociente de " + num1 + " y " + num2 + " es: " + cociente);
        System.out.println("El resto de " + num1 + " y " + num2 + " es: " + resto);
    }
}
