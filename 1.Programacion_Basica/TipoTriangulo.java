//Crea un programa que permita al usuario ingresar la longitud de los lados de un triángulo y determine si es equilátero, isósceles o escaleno.

import java.util.Scanner;
public class TipoTriangulo {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingresa la longitud del primer lado: ");
        double lado1 = input.nextDouble();

        System.out.print("Ingresa la longitud del segundo lado: ");
        double lado2 = input.nextDouble();

        System.out.print("Ingresa la longitud del tercer lado: ");
        double lado3 = input.nextDouble();

        if (lado1 == lado2 && lado2 == lado3) {
            System.out.println("El triángulo es equilátero.");
        } else if (lado1 == lado2 || lado1 == lado3 || lado2 == lado3) {
            System.out.println("El triángulo es isósceles.");
        } else {
            System.out.println("El triángulo es escaleno.");
        }

        input.close();
    }
}
