//Crea un programa que permita al usuario ingresar dos números enteros y los multiplique.

import java.util.Scanner;
public class Multiplicacion {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingresa el primer número: ");
        int num1 = scanner.nextInt();

        System.out.print("Ingresa el segundo número: ");
        int num2 = scanner.nextInt();

        int resultado = num1 * num2;

        System.out.println("La multiplicación de " + num1 + " y " + num2 + " es: " + resultado);
    }
}
