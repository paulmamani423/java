//Crea un programa que permita al usuario ingresar el radio de un círculo y calcule su área y su circunferencia.

import java.util.Scanner;
public class AreaCircunferencia {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingresa el radio del círculo: ");
        double radio = input.nextDouble();

        double area = Math.PI * Math.pow(radio, 2);
        double circunferencia = 2 * Math.PI * radio;

        System.out.println("El área del círculo es: " + area);
        System.out.println("La circunferencia del círculo es: " + circunferencia);

        input.close();
    }
}
