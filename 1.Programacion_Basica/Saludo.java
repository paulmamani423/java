//Crea un programa que permita al usuario ingresar su nombre y lo salude en la consola.

import java.util.Scanner;
public class Saludo{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Por favor, ingrese su nombre: ");
        String nombre = scanner.nextLine();
        saludar(nombre);
    }
    
    public static void saludar(String nombre) {
        System.out.println("Hola, " + nombre + " Buenos dias ");
    }
}
